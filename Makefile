
SHELL := /bin/bash

push:
	AWS_PROFILE=dpa aws s3 sync --acl=public-read --exclude=".git/*" ./ s3://demo.dpa-newslab.com/embed-tester/
